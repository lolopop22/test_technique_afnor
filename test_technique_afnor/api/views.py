from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.forms.models import model_to_dict

from importDataApp.models import DocumentDetail


def get_doc(request, doc_key):
    """
     Retrieves the document(s) that has/have the parameter <doc_key> as key.

    GET Parameters:
         doc_key: key of the document in the database. I supposed that the key was the <numdos> field of a document.

    Returns:
         {
             "success": True if successful, False otherwise,
             "documents": a list containing all the documents having the given <doc_key> as key in the database,
                         an empty list otherwise,
         }
    """

    res = {"success": False, "documents": []}
    status_code = 200
    if request.method == "GET":
        try:
            doc = DocumentDetail.objects.get(numdos=doc_key)
            doc_dict = model_to_dict(doc)
            print(type(doc))
            res = {"success": True, "documents": [doc_dict]}
        except ObjectDoesNotExist:
            print(f"The document with key {doc_key} doesn't exist.")
            status_code = 404
    return JsonResponse(res, status=status_code)


def get_docs(request):
    """
    Retrieves all documents stored in the database.

    No GET Parameters

    Returns:
        {
            "success": True if successful, False otherwise,
            "documents": a list containing all the documents, an empty list otherwise,
        }
    """

    res = {"success": False, "documents": None}
    if request.method == "GET":
        docs = DocumentDetail.objects.all().values()
        if docs:
            res = {"success": True, "documents": list(docs)}
        else:
            res = {"success": True, "documents": []}
    return JsonResponse(res)
