import pytest
import json

from django.test import RequestFactory
from django.urls import reverse

from mixer.backend.django import mixer

from api.views import get_doc, get_docs


@pytest.mark.django_db
class TestViews:
    def test_get_existing_doc(self):
        mixer.blend("importDataApp.DocumentDetail", numdos="test_existing_doc_key")
        path = reverse("get_doc", kwargs={"doc_key": "test_existing_doc_key"})
        request = RequestFactory().get(path)

        response = get_doc(request, doc_key="test_existing_doc_key")
        byte_response = response.content
        json_response = json.loads(byte_response)
        success = json_response.get("success", None)
        docs = json_response.get("documents", None)

        assert response.status_code == 200 and len(docs) == 1 and success is True

    def test_get_docs(self):
        test_docs = mixer.cycle(5).blend(
            "importDataApp.DocumentDetail",
            numdos=mixer.sequence("test_existing_doc_key{0}"),
        )

        path = reverse("get_docs")
        request = RequestFactory().get(path)

        response = get_docs(request)
        byte_response = response.content
        json_response = json.loads(byte_response)
        success = json_response.get("success", None)
        docs = json_response.get("documents", None)

        assert (
            response.status_code == 200
            and len(docs) == len(test_docs)
            and success is True
        )

    def test_get_not_existing_doc(self):
        path = reverse("get_doc", kwargs={"doc_key": "test_not_existing_doc_key"})
        request = RequestFactory().get(path)

        response = get_doc(request, doc_key="test_not_existing_doc_key")

        assert response.status_code == 404

    def test_get_docs_with_empty_db(self):
        path = reverse("get_docs")
        request = RequestFactory().get(path)

        response = get_docs(request)
        byte_response = response.content
        json_response = json.loads(byte_response)
        success = json_response.get("success", None)
        docs = json_response.get("documents", None)

        assert response.status_code == 200 and len(docs) == 0 and success is True
