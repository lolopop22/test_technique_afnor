import pytest

from django.urls import reverse, resolve

from mixer.backend.django import mixer


@pytest.mark.django_db
class TestUrls:
    def test_get_docs_url(self):
        path = reverse("get_docs")
        print(path)
        assert resolve(path).view_name == "get_docs"

    def test_get_doc_url(self):
        mixer.blend("importDataApp.DocumentDetail", numdos="test_doc_key")
        path = reverse("get_doc", kwargs={"doc_key": "test_doc_key"})
        print(path)
        assert resolve(path).view_name == "get_doc"
