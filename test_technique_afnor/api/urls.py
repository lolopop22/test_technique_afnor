from django.urls import path
from . import views

urlpatterns = [
    path("get_docs/", views.get_docs, name="get_docs"),
    path("get_doc/<str:doc_key>", views.get_doc, name="get_doc"),
]
