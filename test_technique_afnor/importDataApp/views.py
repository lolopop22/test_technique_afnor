import csv

from django.shortcuts import render
from django.contrib import messages
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage

from .forms import UploadFileForm
from .models import DocumentDetail

fs = FileSystemStorage(
    location="uploadedDataFolder/"
)  # we use the built-in filesystem storage


def load_in_db(file):
    """
    imports the data contained in the parameter <file> in the sqlite db.
    :param:
        file: file-like object coming from the submitted form

    Returns:
        True if the imports was successful, False otherwise
    """

    print("loading...")
    try:
        content = file.read()  # read the file first
        print("content:")
        print(type(content))
        print(content)
        file_content = ContentFile(content)
        print("File content:")
        print(type(file_content))
        print(file_content)
        file_name = fs.save(
            file.name, file_content
        )  # store the file  on our server in the location specified in the FileSystemStorage
        tmp_file = fs.path(file_name)  # temporary file

        data_list = []
        with open(tmp_file, "r") as tmp_csv_file:
            print("tmp_csv_file")
            print(type(tmp_csv_file))
            print(tmp_csv_file)

            data = csv.reader(tmp_csv_file)
            next(data)  # skips the header

            for id_, row in enumerate(data):
                (numdos, numdosverling, ancart, filiere, etape, verling, format) = row[
                    0
                ].split("|")
                data_list.append(
                    DocumentDetail(
                        numdos=numdos,
                        numdosverling=numdosverling,
                        ancart=ancart,
                        filiere=filiere,
                        etape=etape,
                        verling=verling,
                        format=format,
                    )
                )

            DocumentDetail.objects.bulk_create(data_list)
    except Exception as e:
        print(f"An error occurred.\n Exception: {e}")
        data_list = []

    if data_list:
        return True
    else:
        return False


def index(request):
    return render(request, "index.html")


# Create your views here.
def upload_data(request):
    """
    Uploads the submitted file.
    """
    try:
        if request.method == "POST":
            print("in request function")
            print(request.FILES)
            form = UploadFileForm(request.POST, request.FILES)
            if form.is_valid():
                print("valid form")
                print("file :")
                print(request.FILES["file"])
                print(type(request.FILES["file"]))
                status = load_in_db(request.FILES["file"])
                if status:
                    status_code = 200
                    messages.success(
                        request, "The import was a success!", fail_silently=True
                    )  # without the fail_silently parameter set to True the corresponding testsuite fail
                else:
                    status_code = 500
                    messages.error(request, "The import failed...", fail_silently=True)
                pass
            else:
                print("invalid form")
                status_code = 500
        return render(
            request,
            "index.html",
            context={"form": form},
            status=status_code,
        )
    except Exception as e:
        print(f"something went wrong.\n Exception: {e}")
        messages.error(request, "Something went wrong.", fail_silently=True)
        return render(
            request,
            "index.html",
            context={"form": form},
            status=500,
        )
