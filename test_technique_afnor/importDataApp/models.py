from django.db import models


class DocumentDetail(models.Model):
    numdos = models.CharField(max_length=10)
    numdosverling = models.CharField(max_length=10)
    ancart = models.CharField(max_length=25, null=True)
    filiere = models.CharField(max_length=3)
    etape = models.CharField(max_length=5)
    verling = models.CharField(max_length=7, null=True)
    format = models.CharField(max_length=12, null=True)

    class Meta:
        verbose_name_plural = "Document Details"
