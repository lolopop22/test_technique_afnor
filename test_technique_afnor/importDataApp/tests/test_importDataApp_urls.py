import pytest

from django.urls import reverse, resolve


@pytest.mark.django_db
class TestUrls:
    def test_index_url(self):
        path = reverse("home")
        print(path)
        assert resolve(path).view_name == "home"

    def test_upload_data_url(self):
        path = reverse("upload_data")
        print(path)
        assert resolve(path).view_name == "upload_data"
