import os

import pytest

from django.test import RequestFactory
from django.urls import reverse
from django.core.files.uploadedfile import UploadedFile, SimpleUploadedFile

from PIL import Image

from importDataApp.views import index, upload_data
from importDataApp.forms import UploadFileForm


@pytest.mark.django_db
class TestViews:
    def test_index(self):
        path = reverse("home")
        request = RequestFactory().get(path)

        response = index(request)

        assert response.status_code == 200

    def test_upload_good_dataset(self):
        filepath = os.path.join(
            os.getcwd(), "importDataApp/tests/import_data_test/good_dataset.csv"
        )
        # post_data = {'file':filepath}
        with open(filepath, "r") as f:
            post_data = {"file": UploadedFile(file=f, name="good_dataset.csv")}

            path = reverse("upload_data")
            request = RequestFactory().post(path, data=post_data)

            response = upload_data(request)

            assert response.status_code == 200

    def test_upload_bad_file(self):
        filepath = os.path.join(
            os.getcwd(), "importDataApp/tests/import_data_test/img.jpg"
        )
        im = Image.open(filepath)
        post_data = {"file": SimpleUploadedFile(content=im.tobytes(), name="img.jpg")}

        path = reverse("upload_data")
        request = RequestFactory().post(path, data=post_data)

        response = upload_data(request)

        assert response.status_code == 500
