from django.urls import path
from . import views

urlpatterns = [
    path("home", views.index, name="home"),
    path("upload_data/", views.upload_data, name="upload_data"),
]
