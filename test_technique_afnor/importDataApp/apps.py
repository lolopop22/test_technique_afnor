from django.apps import AppConfig


class ImportdataappConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "importDataApp"
