from django.contrib import admin
from .models import DocumentDetail


admin.site.register(DocumentDetail)
